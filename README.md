# Whatsupp



## Getting started

You can clone the full stack application through the following git command.

```
git clone git@gitlab.com:naman.alireza/whatsupp.git
```

## Installation
The installation steps for frontend and backend are described separately within their corresponding folders.


## Project status
The project is just a demonstration of a real-time chat application written in Node.js, React, and Typescript based on GraphQL. 


Some notes about the project:

- The code is written with minimal dependencies, as overall design, structure, and readability are considered as the main goals of the project.
- Users are able to create channels and create usernames to join the real-time messaging channels. 
- Channels and usernames will be removed each time reseting the node server, as there are no separate databases involved in the project. 
- Users can access the chat app by entering channel and username and authentication is not needed. Make sure to use exactly the same username and channel name when you want to join the chat.
- Mobile version is not designed, please use a large screen to be able to get the best view.
- I'm aware that inline-css is againts best practises. I don't use inline-css in large projects.


*If you have any question or issues, please send me an email to naman.alireza@gmail.com




