# Getting Started


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

The page will reload if you make edits.\
You will also see any errors in the console.

## Learn More

You can learn more in the [Node.js Documentation](https://nodejs.org/en/docs/guides/getting-started-guide/).
