import { GraphQLServer, PubSub } from "graphql-yoga";
import Query from "./resolvers/Query";
import Mutation from "./resolvers/Mutation";
import Subscription from "./resolvers/Subscription";
import messages from './context/Message';

const pubsub = new PubSub();
const server = new GraphQLServer({
  typeDefs: './src/typedefs/schema.graphql',
  resolvers: {
    Query,
    Mutation,
    Subscription
  },
  context: { messages, pubsub },
});

server.start(({ port }) => {
  console.log(`Server on http://localhost:${port}/`);
});
