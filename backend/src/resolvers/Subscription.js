const Subscription = {
  messages: {
    subscribe: (parent, { channel }, { messages, pubsub }) => {
      setTimeout(
        () =>
          pubsub.publish(channel, {
            messages: messages.filter((m) => m.channel === channel),
          }),
        0
      );
      return pubsub.asyncIterator(channel);
    },
  },
};

export default Subscription;
