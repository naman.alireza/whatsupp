const Query = {
    messages: (parent, {channel}, {messages}) => {
        return messages.filter(m => m.channel === channel);
    }
}

export default Query;