import { v4 as uuidv4 } from "uuid";

const Mutation = {
  postMessage: (parent, { user, text, channel }, { messages, pubsub }) => {
    const id = uuidv4();
    const message = {
      id,
      user,
      text,
      channel,
    };
    messages.push(message);
    pubsub.publish(channel, {
      messages: messages.filter((m) => m.channel === channel),
    });
    return id;
  },
};

export default Mutation;
