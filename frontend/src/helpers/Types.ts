export type MessageType = {
    id?: string;
    user: string;
    text: string;
    channel: string;
};

export type MessageProps = {
    key?: string;
    user: string;
    text: string;
    fromMe: boolean;
    last: boolean;
}
