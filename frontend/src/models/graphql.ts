import { gql } from "@apollo/client";

export const GET_CONVERSIONS = gql`
  subscription($channel: String!) {
    messages(channel: $channel) {
      id
      user
      text
      channel
    }
  }
`;

export const POST_MESSAGE = gql`
  mutation ($user: String!, $text: String!, $channel: String!) {
    postMessage(user: $user, text: $text, channel: $channel)
  }
`;
