import { Container, Navbar, Button } from "react-bootstrap";
import "./ChannelHeader.css";

const ChannelHeader: React.FC<{
  user: string;
  channel: string;
  onLogout: () => void;
}> = ({ user, channel, onLogout }) => {
  return (
    <Navbar
      collapseOnSelect
      expand="lg"
      bg="dark"
      variant="dark"
      className="channel-header"
    >
      <Container>
        <Navbar.Brand href="#home">
          {channel}{" "}
          <small className="channel-header__username">
            Signed in as: {user}
          </small>{" "}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Button variant="secondary" onClick={() => onLogout()}>
          Logout
        </Button>
      </Container>
    </Navbar>
  );
};

export default ChannelHeader;
