import Conversions from './Conversions';
import SendMessage from './SendMessage';
import ChannelHeader from './ChannelHeader';
import './Channel.css';

const Channel: React.FC<{ user: string, channel: string, onLogout: () => void }> = ({user, channel, onLogout}) => {
    
    return (
        <>
        <ChannelHeader user={user} channel={channel} onLogout={onLogout}/>
        <Conversions user={user} channel={channel}/>
        <SendMessage user={user} channel={channel}/>
        </>
    )
}


export default Channel;
