import { useState } from "react";
import { useMutation } from "@apollo/client";
import { MessageType } from "../helpers/Types";
import { Form, FormControl, InputGroup, Button } from "react-bootstrap";
import { POST_MESSAGE } from "../models/graphql";

const SendMessage: React.FC<{ user: string, channel: string }> = ({ user, channel }) => {
  const [text, setText] = useState("");

  const [postMessage] = useMutation(POST_MESSAGE);

  const submitHandler: React.FormEventHandler<HTMLFormElement> = (
    e: React.FormEvent<HTMLFormElement>
  ): void => {
    e.preventDefault();
    if (text.length > 0) {
      const message: MessageType = {
        user: user,
        text: text,
        channel: channel
      }
      postMessage({
          variables: message
      })
    }
    setText("");
  };

  return (
    <>
      <Form onSubmit={submitHandler}>
        <InputGroup>
            <FormControl as="textarea"
            rows={3}
            required
            value={text}
            onChange={(e) => setText(e.target.value)}
            style={{ height: "72px", resize: "none", borderRadius: 0, border: 'none', borderTop: '1px solid #ddd'}} />
            <Button variant="primary" type="submit" className="rounded-0">Send</Button>
        </InputGroup>
      </Form>
    </>
  );
};

export default SendMessage;
