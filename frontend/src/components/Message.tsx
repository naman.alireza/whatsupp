import "./Message.css";
import { MessageProps } from "../helpers/Types";

const Message: React.FC<MessageProps> = ({ key, user, text, fromMe, last }) => {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: fromMe ? "flex-end" : "flex-start",
        paddingBottom: "1em",
      }}
      key={key}
    >
      <div
        style={{
          background: fromMe ? "#03a9f4" : "#e4e6e8",
          color: fromMe ? "white" : "black",
          padding: "1em",
          paddingTop: "0.8rem",
          borderRadius: "1em",
          maxWidth: "60%",
        }}
      >
        { !fromMe && (
          <span className="w-100 message__from">{user}</span>
        )}
        {text}
      </div>
    </div>
  );
};

export default Message;
