import React from "react";
import { useSubscription } from "@apollo/client";
import { MessageType } from "../helpers/Types";
import Message from "./Message";
import { GET_CONVERSIONS } from "../models/graphql";


const Conversions: React.FC<{ user: string; channel: string }> = ({
  user,
  channel,
}) => {
  const { data } = useSubscription(GET_CONVERSIONS, {
    variables: { channel },
  });

  return (
    <>
      <div
        style={{
          padding: "25px",
          height: "calc(90vh - 130px)",
          overflowY: "scroll",
        }}
      >
        {data && data.messages.map((message: MessageType, index: number) => {
          
          const isLast = data.messages.length - 1 === index;

          return (
            <Message key={message.id} user={message.user} text={message.text} fromMe={user === message.user} last={isLast}/>
          );
        })}
      </div>
    </>
  );
};

export default Conversions;
