import React, { useRef } from 'react'
import { Container, Form, Button } from 'react-bootstrap'
// import { v4 as uuidV4 } from 'uuid'


 const Login: React.FC<{ user: String, channel: string, onLogin: any }> = ({ user, channel, onLogin }) => {
  const usernameRef = useRef<HTMLInputElement>(null)

  function handleSubmit(e: any) {
    e.preventDefault()
    const username = usernameRef.current?.value
    if(username){
        onLogin(usernameRef.current.value)
    }
  }

  return (
    <Container className="d-flex flex-column justify-content-center" style={{ height: '100vh', padding: '0 20px'}}>
      <div>
        <h1 className="mb-20 logo">Whatsupp</h1>  
        <h1 className="mb-50 text-center">Join channel: {channel}</h1>
        <Form onSubmit={handleSubmit} className="w-100">
            <Form.Group>
                <Form.Label>Enter a username</Form.Label>
                <Form.Control type="text" ref={usernameRef} required size="lg"/>
                <small className="input__guide">If the username does not exist, a new one will be created</small>

                <Button type="submit" className="mt-5 w-100" size="lg">Join</Button>
            </Form.Group>
            
        </Form>
      </div>
    </Container>
  )
}


export default Login;