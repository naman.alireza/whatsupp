import React, { useRef } from 'react'
import { Container, Form, Button } from 'react-bootstrap'

const CreateChannel: React.FC<{ onCreateChannel: (channel: string) => void }> = ({onCreateChannel}) => {
  const channelRef = useRef<HTMLInputElement>(null)

  function handleSubmit(e: any) {
    e.preventDefault()
    const channel = channelRef.current?.value
    if(channel){
        onCreateChannel(channelRef.current.value)
    }
  }

  return (
    <Container className="align-items-center d-flex" style={{ height: '100vh' }}>
      <Form onSubmit={handleSubmit} className="w-100">
        <Form.Group>
          <Form.Label>Enter a channel name</Form.Label>
          <small className="input__guide">(If the channel does not exist, a new one will be created)</small>
          <Form.Control type="text" ref={channelRef} required size="lg"/>
        </Form.Group>
        <Button type="submit" className="mt-5 w-100" size="lg">Join</Button>
      </Form>
    </Container>
  )
}

export default CreateChannel;