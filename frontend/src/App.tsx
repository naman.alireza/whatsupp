import "./App.css";
import { ApolloProvider } from "@apollo/client";
import { APOLLO_CLIENT } from "./helpers/apollo";
import useLocalStorage from "./hooks/useLocalStorage";
import JoinChannel from './components/JoinChannel';
import Channel from './components/Channel';
import Login from './components/Login';


function App() {
  const [user, setUser] = useLocalStorage('user', '')
  const [channel, setChannel] = useLocalStorage('channel', '')

  const logoutHandler = () => {
    setUser('');
    setChannel('');
  }

  return (
    <ApolloProvider client={APOLLO_CLIENT}>

      { !channel && <JoinChannel onCreateChannel={setChannel}/> }

      { channel && !user && <Login user={user} channel={channel} onLogin={setUser}/>}
      
      { channel && user && <Channel user={user} channel={channel} onLogout={logoutHandler}/>}

    </ApolloProvider>
  );
  
}

export default App;
